/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package test;

/**
 *
 * @author ri
 */
public class Tree {
    
    Tree()
    {
        head = null;
    }
    
    Boolean add(int val)
{
    if(head == null)
    {
        head = new Node(val);
        return true;
    }

    return insert(val, head);
}
    
    Boolean insert(int val, Node vertex)
{
    if(val == vertex.val)
        return false;
    else if(val < vertex.val)
    {
        if(vertex.left != null)
        
            return insert(val, vertex.left);
        else
        {
            vertex.left =new Node(val);
            return true;
        }
    }
    else
    {
        if(vertex.right != null)
            return insert(val, vertex.right);
        else
        {
            vertex.right =new Node(val);
            return true;
        }
    }
}

    
    
    private Node head;
    
    
    void print()
{
    System.out.println("Your tree is");
    if(head != null)
        head.print(0);

}

    Boolean empty()
{
    return (head == null);
}
    
}
