/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package test;

/**
 *
 * @author ri
 */
public class Node {
    
    Node(int v)
    {
        val = v;
        right = null;
        left = null;
    }
    
    public void print (int margin){
        for (int i = 0; i< margin;i++ )
            System.out.print("---");
        System.out.println(val);
        if(right != null)
            right.print(margin+1);
        else
            printEmptyEdge(margin);
        if(left != null)
            left.print(margin+1);
        else
            printEmptyEdge(margin);
                    
        
    }
    
    private void printEmptyEdge(int margin){
        for(int i = 0; i < margin; i++)
            System.out.print("---");
        System.out.println();          
    }
   public int val;
   public Node right;
   public Node left;
}
